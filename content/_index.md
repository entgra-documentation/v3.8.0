---
title: Introduction
type: docs
---

# Overview

[Entgra IoT Server](http://entgra.io/) provides the essential capabilities required to implement a scalable server-side IoT Platform. These capabilities involve device management, API/App management for devices, analytics, customizable web portals, transport extensions for MQTT, XMPP and much more. Entgra IoT Server contains sample device agent implementations for well-known development boards and platforms, such as Arduino UNO, Raspberry Pi, Android, iOS, Windows and Virtual agents that demonstrate various capabilities. Furthermore, Entgra IoT Server is released under the Apache Software License Version 2.0, one of the most business-friendly licenses available today.

*   Do you like to contribute to [Entgra IoT Server](http://entgra.io/) and get involved with the Entgra developer community? For more information, see how you can **[participate in the Entgra community](http://entgra.io/community)**.
*   Do you need a customized device type that will run on Entgra IoT Server for research or business requirements? We will be glad to assist you so **[send us your requirement](http://entgra.io/)**.

Internet of Things is the concept of using devices to collect, analyze and exchange data that is valuable to an end-user. Entgra IoT Server has the capability to manage devices and use the data gathered by these devices to derive key and useful information. The Entgra IoT platform is a combination of the following areas:

*   **Core offering**   
    The IoT Server core offering is centralized around device management focusing on device plugins, event stream management and more.
*   **IoT Analytics**   
    The data gathered via the devices are analyzed to produce information that will be useful to the end-user.
*   **Extended Platform **  
     Entgra IoT Server can then be extended so that it can be used with the integration, machine learning, workflows and many other areas.    
    For example, the extended platform will involve the WSO2 Business Process Server (BPS) to handle the workflows and business processors in an organization.
