---
bookCollapseSection: true
weight: 1
---

# Enroll Android as a dedicated device using QR code

{{< hint info >}}
<strong>Pre-requisites</strong><br>
<ul style="list-style-type:disc;">
    <li>Server is <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a></li>
    <li>Logged into the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">device mgt portal</a></li>
    <li>Optionally, basic <a href="{{< param doclink >}}key-concepts/#android ">concepts of Android device management</a> will be beneficial as well. </li>
</ul>
{{< /   hint >}}



<iframe width="560" height="315" src="https://www.youtube.com/embed/jWaFF5p_epw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Set platform configurations</li>
    <li>Click "Enroll Device" and select device type</li>
    <li>In the server Select device ownership as "COSU (KIOSK)"</li>
    <li>Tap 6 times on the welcome screen after factory reset (Only works for devices on and above android 9.0)</li>
    <li>Scan QR code that is generated in server</li>
    <li>Check license agreement and privacy policy then click "Next"</li>
</ul>
