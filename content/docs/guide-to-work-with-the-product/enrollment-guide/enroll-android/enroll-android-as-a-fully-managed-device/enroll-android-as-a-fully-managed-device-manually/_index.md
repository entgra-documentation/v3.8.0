---
bookCollapseSection: true
weight: 1
---

# Enroll Android as a fully managed device manually

{{< hint info >}}
<strong>Pre-requisites</strong>
<ul style="list-style-type:disc;">
    <li>Server is <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a></li>
    <li>Logged into the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">device mgt portal</a></li>
    <li>Please follow <a href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-android/install-agent/ "> install agent section </a></li>
    <li>Optionally, basic <a href="{{< param doclink >}}key-concepts/#android ">concepts of Android device management</a> will be beneficial as well. </li>
</ul>
{{< /hint >}}

<iframe width="560" height="315" src="https://www.youtube.com/embed/BkTuTinbdm0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Enter the following command on terminal or console to enable device owner.
        <br><i>adb shell 
    dpm 
    set-device-owner io.entgra.iot.agent/org.wso2</i> .iot.agent.services.AgentDeviceAdminReceiver
    </li>
    <li>Open the agent application</li>
    <li>Click continue when disclaimer appears</li>
    <li>In the next screen click "Manual Enrollment"</li>
    <li>In the next screen click "COPE Enrollment"</li>
    <li>Type in the server address which is the IP of the server and port as 8280. Click start registration</li>
    <li>When the policy agreement is shown, click "Agree" to proceed</li>
    <li>Type the Username: admin, Password: admin then click "sign in"</li>
    <li>When the policy agreement is shown, click "Agree" to proceed</li>
    <li>User will be prompted to agree to few permissions and click "Allow"</li>
    <li>Agree to using data usage monitoring to allow server to check the data usage of the device</li>
    <li>Allow agent to change do not disturb status which is used to ring the device</li>
    <li>Enter and confirm a PIN code, which will be needed by admin to perform any critical tasks with user concent. Then click "Set PIN Code" to complete enrollment</li>
    <li>In the device screen click "Agree" to accept the licence</li>
</ul>