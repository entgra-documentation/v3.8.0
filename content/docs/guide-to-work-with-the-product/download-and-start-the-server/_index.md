---
bookCollapseSection: true
weight: 2
---
# Download and start the server 

This section explains how to do some basic configurations and start up the server. Before proceeding, make sure to read the <a href="{{< param doclink >}}guide-to-work-with-the-product/system-requirements/">system requirements</a> page and have an environnement ready.
This guide uses a linux based OS.

## Get Entgra IoT server

Go to [Entgra contact us](https://entgra.io/contact) fill in the details to get a copy of the server. If iOS or windows platforms are needed to be evaluated, please mention this need as, there are separated plugins for these platforms that is not bundled with the server.

## Extract zip file

Extract the zip file to a desired location and go into the extracted folder(This will be in format entgra-iot-4.x.x). Lets call this folder IOT_HOME folder.

## Change IP

If you are setting this up on your personal PC/laptop, This step is not mandatory, but there is no harm in following this step.

1. Using a command prompt, go to IOT_HOME/scripts folder. For example like bellow,```cd /Users/username/Downloads/entgra-iot-3.7.0/scripts/```
2. And execute change-ip.sh. This can be done with following command,```./change-ip.sh```
3. There are 3 critical values to fill in when executing change-ip.

<table class="tableizer-table">
<thead><tr class="tableizer-firstrow"><th>Input</th><th>Description</th></tr></thead><tbody>
 <tr><td>Please enter the IoT Core IP that you need to replace (if you are trying out IoT server for the first time this will be localhost)</td><td>Provide this as localhost</td></tr>
 <tr><td>Please enter your current IP</td><td>Enter the machine IP or the public IP to the machine. This IP must be accessible within the network or over internet. This is because the device we are enrolling later on should be able to access this IP.</td></tr>
 <tr><td>Please provide Common Name</td><td>Same as current IP</td></tr>
</tbody></table>

For all the other steps are optional and can press enter to set default value.

## Start the server

To start the server, go to, IOT_HOME/bin and run ./iot-server.sh


You may also start this as a service with ```./iot-server.sh start```
Stop the service with ```./iot-server.sh stop```
If you have started as a service, the startup logs can be monitored with the command,
```tail -f repository/logs/wso2carbon.log```

Server start up will end with publishing APIs.



