---
bookCollapseSection: true
weight: 3
---

# Encryption Settings
{{< hint info >}}
<b> <a href ="https://entgra-documentation.gitlab.io/v3.7
.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

This configuration can be used to encrypt data on an Android device, when the device is locked and make it readable when the passcode is entered. Once this configuration profile is installed on a device, corresponding users will not be able to modify these settings on their devices.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Enable storage encryption</strong></td>
            <td>Encryption is the process of encoding all user data on an Android device using symmetric encryption keys.
            Having this checked would enable Storage-encryption in the device.
            </td>
        </tr>     
    </tbody>
</table>

<img src ="encrypt_short.gif" style="border:5px solid black">

{{< hint info >}}
<b> <a href = "https://entgra-documentation.gitlab.io/v3.7
.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device
-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}