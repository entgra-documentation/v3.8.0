---
bookCollapseSection: true
weight: 1
---

# Passcode Policy

{{< hint info >}}
<b> <a href ="https://entgra-documentation.gitlab.io/v3.7
.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

Enforce a configured passcode policy on Android devices. Once this profile is applied, the device 
owners won't be able to modify the password settings on their devices.

<i> Please note that * sign represents required fields of data </i>


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Allow simple value</strong></td>
            <td>Permits repeating, ascending and descending character sequences.
            </td>
        </tr>
        <tr>
            <td><strong>Allow alphanumeric value</strong></td>
            <td>The user must enter a password containing at least both numeric and alphabetic (or other symbol) characters.</td>
        </tr>
        <tr>
            <td><strong>Minimum passcode length</strong></td>
            <td>Set the required number of characters for the password. For example, you can require PIN or passwords to have at least six characters.</td>
        </tr>
        <tr>
            <td><strong>Minimum number of complex characters</strong></td>
            <td>Set the required number of letters, numericals digits, and special symbols that passwords must contain. Introduced in Android 3.0.
        </tr>
        <tr>
            <td><strong>Maximum passcode age in days</strong>
                <br> ( Should be in between 1-to-730 days or 0 for none )</td>
            <td>Designates the full email address for the account. If not present in the payload, the device prompts for this string during profile installation.</td>
        </tr>
        <tr>
            <td><strong>Passcode history</strong>
                <br> ( Should be in between 1-to-50 passcodes or 0 for none )</td>
            <td>Number of consequent unique passcodes to be used before reuse</td>
        </tr>
        <tr>
            <td><strong>Maximum number of failed attempts</strong></td>
            <td>Specifies how many times a user can enter the wrong password before the device wipes its data. The Device Administration API also allows administrators to remotely reset the device to factory defaults. This secures data in case the device is lost or stolen.</td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "https://entgra-documentation.gitlab.io/v3.7
.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device
-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}
