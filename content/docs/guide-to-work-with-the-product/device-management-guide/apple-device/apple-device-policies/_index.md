---
bookCollapseSection: true
weight: 3
---

# Apple device policies

## Add a policy

{{< hint info >}}
   <strong>Pre-requisites</strong>
   <br>
   <ul style="list-style-type:disc;">
       <li>Server is <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a></li>
       <li>Logged into the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">device mgt portal</a></li> 
   </ul>
   {{< /   hint >}}

1.Click on Add policies (https://{IP}:{port}/devicemgt/policy/add) 

![image](11.png)

2.Click on iOS from "DEVICE TYPES"

![image](12.png)

3.Create your policy. In this tutorial, let's create a passcode policy.
   After defining the settings, click CONTINUE.
   
      A profile in the context of WSO2 IoT Server refers to a collection of policies.
      For example, in this use case you are only creating one policy that is the passcode policy.
      If you want to, you can add an restrictions policy too. 
      All these policies will be bundled as a profile and then pushed to the devices.
   
4.Define the user groups that the passcode policy needs to be assigned to:
      
      Select the set user role/s or set user/s option and then select the users/roles from the item 
      list.
      Let's select set user role/s and then select ANY. 
      
![image](13.png)

5.Click CONTINUE.

6.Define the policy name and the description of the policy.

![image](14.png)

7.Click SAVE AND PUBLISH to save and publish the configured profile as an active policy to the 
database.
           
    If you SAVE the configured profile, it will be in the inactive state and will not be applied 
    to any devices. 
    If you SAVE AND PUBLISH the configured profile of policies, it will be in the active state.   
8.To publish the policy to the existing devices, click APPLY CHANGES TO DEVICES from the policy 
  management page.
  
  
## View a policy

1.Go to devicemgt portal and click on View policies 
(https://{IP}:{port}/devicemgt/devicemgt/policies
![image](17.png)

## Publish a policy

1.Click View under POLICIES to get the list of the available policies.

![image](17.png)

2.Click Select to select the policy or policies that are not in the publish state and you wish to 
publish.

![image](22.png)

3.Click Publish.

![image](21.png)

## Unpublish a policy

1.Go to devicemgt portal and click on View policies 
(https://{IP}:{port}/devicemgt/devicemgt/policies

![image](17.png)

2.Click Select to select the policy or policies that are not in the publish state and you wish to 
publish.

![image](19.png)

3.Click Unpublish

![image](23.png)

4.Click YES to confirm that you want to unpublish the policy.

![image](24.png)

5.Now your policy is unpublished and is in the inactive/updated state. Therefore, the policy will
 not be applied on devices that enroll newly with Entgra IoT Server.
 
![image](25.png)

## Verify the policy enforced on a device

1.Click View under DEVICES

![image](26.png)

2.Click on your device to view the device details. Click Policy Compliance.

3.You will see the policy that is currently applied to your device.

## Manage the policy priority order

You can change the priority order of the policies and make sure the policy that you want is applied 
on devices that register with Entgra IoT Server. 

1.Click View under POLICIES to get the list of the available policies.

![image](17.png)

2.Click POLICY PRIORITY.

![image](27.png)

3.Manage the policy priority:
    Drag and drop the policies to prioritize the policies accordingly.
    Manage the policy priority order by defining the order using the edit box.   
    ![image](28.png)
    
4.Click SAVE NEW PRIORITY ORDER to save the changes. 

5.Click APPLY CHANGES to push the changes, to the existing devices.

## Updating a Policy

1.Click View under POLICIES to get the list of the available policies.

![image](17.png)

2.On the policy, you wish to edit, click on the edit icon.

![image](29.png)

3.Edit the policy:

    a.Edit current profile and click CONTINUE.
    b.Edit assignment groups and click CONTINUE.   
    c.Optionally, edit the policy name and description.
    
  Click SAVE to save the configured profile or click SAVE AND PUBLISH to save and publish the 
  configured profile as an active policy to the database.
  
<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th>Policy</th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong><a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/passcode-policy/">Passcode 
              Policy</a></strong></td>
            <td>This configuration can be used to set a passcode policy to an iOS Device. Once this configuration profile is installed on a device, corresponding users will not be able to modify these settings on their devices.
            </td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/restrictions/">Restrictions</a></strong></td>
            <td>These configurations can be used to restrict apps, device features and media content available on an iOS device. Once this configuration profile is installed on a device, corresponding users will not be able to modify these settings on their devices.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/wi-fi-policy/">Wi-Fi Settings</a></strong></td>
            <td>These configurations can be used to set how devices connect to your wireless network(s), including the necessary authentication information. Once this configuration profile is installed on an iOS device, corresponding users will not be able to modify these settings on their devices.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/global-proxy-settings/">Global Proxy Settings</a></strong></td>
            <td>Configure a global HTTP proxy to direct all HTTP traffic from Supervised iOS 7 and higher devices through a designated proxy server. Once this configuration profile is installed on a device, all the network traffic will be routed through the proxy server
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/email-settings/">Email Settings</a></strong></td>
            <td>These configurations can be used to define settings for connecting to your POP or IMAP email accounts. Once this configuration profile is installed on an iOS device, corresponding users will not be able to modify these settings on their devices.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/air-play-settings/">AirPlay Settings</a></strong></td>
            <td>This configuration can be used to define settings for connecting to AirPlay destinations. Once this configuration profile is installed on an iOS device, corresponding users will not be able to modify these settings on their devices. (This feature is supported only on iOS 7.0 and later.)</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/manage-domains/">Manage Domains</a></strong></td>
            <td>This payload defines web domains that are under an enterprise’s management.
            </td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/l-d-a-p-settings/">LDAP Settings</a></strong></td>
            <td>This configuration can be used to define settings for connecting to LDAP servers. Once this configuration profile is installed on an iOS device, corresponding users will not be able to modify these settings on their devices.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/active-sync-confirurations/">ActiveSync Configurations</a></strong></td>
            <td>This configuration can be used to provision ActiveSync Configurations for iOS devices.
            </td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/calender/">Calendar</a></strong></td>
            <td>This configuration can be used to define settings for connecting to CalDAV servers. Once this configuration profile is installed on an iOS device, corresponding users will not be able to modify these settings on their devices.
            </td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/cellular-network-settings/">Calendar Subscription</a></strong></td>
            <td>This configuration can be used to define settings for calendar subscriptions. Once this configuration profile is installed on an iOS device, corresponding users will not be able to modify these settings on their devices.
            </td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/cellular-network-settings/">Cellular Network Settings</a></strong></td>
            <td>These configurations can be used to specify Cellular Network Settings on an iOS device. Cellular settings cannot be installed if an APN setting is already installed and upon successful installation, corresponding users will not be able to modify these settings on their devices. (This feature is supported only on iOS 7.0 and later.)</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/network-usage-rules/">Network Usage Rules</a></strong></td>
            <td>This configurations can be used to specify how managed apps use networks.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/virtual-private-network/">Virtual Private Network(VPN) Settings</a></strong></td>
            <td>This configurations can be used to configure VPN settings on an iOS device. Once this configuration profile is installed on a device, corresponding users will not be able to modify these settings on their devices.
            </td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/certificate-install/">Certificate Install</a></strong></td>
            <td>This configurations can be used to install certificate on an iOS device.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/font-install/">Font Install</a></strong></td>
            <td>This configurations can be used to add an additional font to an iOS device.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}">Enrollment Application Install</a></strong></td>
            <td>Enforce applications to be installed during iOS device enrollment. This configuration will be applied only during iOS device enrollment.
            </td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/font-install/">App Lock(Kiosk)</a></strong></td>
            <td>Enforce iOS device to lock to a single application. This configuration will be applied only on Supervised devices.
            </td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/app-store-restriction/">App Store Restriction</strong></td>
            <td>These configurations can be used to restrict the app store on a mac-os device. Once this configuration profile is installed on a device, corresponding users will not be able to access the app store of the device. This configuration will be applied only on macOS devices.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/login-window-preferece/">Login Window Preference</strong></td>
            <td>This payload creates managed preferences on all versions of macOS for system and device profiles. Multiple Login window payloads may be installed together. This configuration will be applied only on macOS devices.</td>
        </tr>
    </tbody>
</table>