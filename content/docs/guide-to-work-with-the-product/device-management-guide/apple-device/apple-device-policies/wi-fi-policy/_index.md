---
bookCollapseSection: true
weight: 3
---
# Wi-Fi Settings

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Service Set Identifier (SSID)</strong></td>
            <td>SSID of the Wi-Fi network to be used.
                <br>In iOS 7.0 and later, this is optional if a DomainName value is provided.
            </td>
        </tr>
        <tr>
            <td><strong>Domain Name</strong></td>
            <td>This field can be provided instead of SSID_STR.
                <br>Available in iOS 7.0 and later.( For Wi-Fi Hotspot 2.0 negotiation )</td>
        </tr>
        <tr>
            <td><strong>Hidden Network</strong></td>
            <td>Besides SSID, the device uses information such as broadcast type and encryption type to differentiate a network. By default (false), it is assumed that all configured networks are open or broadcast. To specify a hidden network, must be true.
            </td>
        </tr>
        <tr>
            <td><strong>Hot Spot</strong></td>
            <td> If true, the network is treated as a hotspot.
                <br>Available in iOS 7.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Enable Service Provider Roaming</strong></td>
            <td>If true, allows connection to roaming service providers. Defaults to false.
                <br>Available in iOS 7.0 and later.
        </tr>
        <tr>
            <td><strong>Auto Join</strong></td>
            <td> If true, the network is auto-joined. If false, the user has to tap the network name to join it.
                <br>Available in iOS 5.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Displayed Operator Name </strong></td>
            <td>The operator name to display when connected to this network. Used only with Wi-Fi Hotspot 2.0 access points.
                <br>Available in iOS 7.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Proxy Setup</strong></td>
            <td> Valid values are None, Manual, and Auto.
                <br>Available in iOS 5.0 and later.
                <br>If the ProxyType field is set to Manual, the following fields must also be provided
                <ul style="list-style-type:disc;">
                    <li><strong>Proxy Server</strong>: The proxy serverʼs network address.( Server URL or IP Address )</li>
                    <li><strong>Proxy Server Port</strong>: The proxy serverʼs port.</li>
                    <li><strong>Proxy Username</strong>: The username used to authenticate to the proxy server.</li>
                    <li><strong>Proxy Password</strong>: The password used to authenticate to the proxy server.</li>
                    <li><strong>Proxy PAC URL</strong>: The URL of the PAC file that defines the proxy configuration.
                    </li>
                    <li><strong>Allow Proxy PAC FallBack</strong>: . If false, prevents the device from connecting directly to the destination if the PAC file is unreachable. Default is false.
                        <br>Available in iOS 7 and later
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td><strong>Encryption Security Type</strong></td>
            <td>Encryption Security Type field is set to WEP, WPA, or ANY, the following fields may also be provided
                <ul style="list-style-type:disc;">
                    <li><strong>Wi-Fi Password</strong>: Password used for encryption security. Absence of a password does not prevent a network from being added to the list of known networks. The user is eventually prompted to provide the password when connecting to that network.</li>
                    <li><strong>EAP Client Configuration</strong>: In addition to the standard encryption types, it is possible to specify an enterprise profile for a given network via the EAP Client Configuration key. If present, its value is a dictionary with the following keys: The following EAP types are accepted:
                        <ul>
                            <li>13 = TLS</li>
                            <li>17 = LEAP</li>
                            <li>18 = EAP-SIM</li>
                            <li>21 = TTLS</li>
                            <li>23 = EAP-AKA</li>
                            <li>25 = PEAP</li>
                            <li>43 = EAP-FAST</li>
                        </ul>
                        For EAP-TLS authentication without a network payload, install the necessary identity certificates and have your users select EAP-TLS mode in the 802.1X credentials dialog that appears when they connect to the network. For other EAP types, a network payload is necessary and must specify the correct settings for the network.</li>
                    <li><strong>Username</strong>: Unless you enter a user name, this property won't appear in an imported configuration. Users can enter this information by themselves when they authenticate.
                    </li>
                    <li><strong>Password</strong>: If not provided, the user will be prompted during login.</li>
                    <li><strong>One Time Password</strong>: If checked, the user will be prompted for a password each time they connect to the network.
                    </li>
                    <li><strong>TLS Trusted Server Certificate Names</strong>: This is the list of server certificate common names that will be accepted. You can use wildcards to specify the name, such as wpa.*.example.com. If a server presents a certificate that isn't in this list, it won't be trusted. Used alone or in combination with TLSTrustedCertificates, the property allows someone to carefully craft which certificates to trust for the given network, and avoid dynamically trusted certificates.
                    </li>
                    <li><strong>Allow TLS Trust Exceptions</strong>: Allows / disallows a dynamic trust decision by the user. The dynamic trust is the certificate dialogue that appears when a certificate isn't trusted. If this is unchecked, the authentication fails if the certificate isn't already trusted.
                    </li>
                    <li><strong>Require TLS Certificate</strong>: If checked, allows for two-factor authentication for EAP-TTLS, PEAP or EAP-FAST. If unchecked, allows for zero factor authentication for EAP-TLS. By default this is enabled for EAP-TLS and disabled for other EAP types. Available in iOS 7.0 and later.
                    </li>
                    <li><strong>TTLS Inner Authentication Type</strong>: Specifies the inner authentication used by the TTLS module. Possible values are PAP, CHAP, MSCHAP and MSCHAPv2.
                    </li>
                    <li><strong>Outer Identity</strong>: This key is only relevant to TTLS, PEAP, and EAP-FAST. This allows the user to hide his or her identity. The userʼs actual name appears only inside the encrypted tunnel. For example, it could be set to ”anonymous” or ”anon”, or ”anon@mycompany.net”. It can increase security because an attacker canʼt see the authenticating userʼs name in the clear.
                    </li>
                    <li><strong>EAP-Fast Support</strong>: <strong>
                        <ul>
                            <li>Use existing PAC for EAP-FAST
                            </li>
                            <li>Allow PAC Provisioning</li>
                            <li>Allow Anonymous PAC Provisioning
                            </li>
                        </ul>
                        </strong>These keys are hierarchical in nature. : If Use existing PAC for EAP-FAST is false, the other two properties arenʼt consulted. Similarly, if Allow PAC Provisioning is false, Allow Anonymous PAC Provisioning isnʼt consulted.
                        <br>If Use existing PAC for EAP-FAST is false, authentication proceeds much like PEAP or TTLS: the server proves its identity using a certificate each time.If checked, the device will use an existing PAC. Otherwise, the server must present its identity using a certificate.
                        <br>If Allow PAC Provisioning is checked, allows PAC provisioning. This particular attribute must be enabled for EAP-FAST PAC usage to succeed, because there is no other way to provision a PAC.
                        <br>If Allow Anonymous PAC Provisioning is checked, provisions the device anonymously. Note that there are known man-in-the-middle attacks for anonymous provisioning.
                    </li>
                    <li><strong>Number of expected RANDs for EAP-SIM</strong>: Number of expected RANDs for EAPSIM. Valid values are 2 and 3. Defaults to 3.
                    </li>
                    <li><strong>Certificate Payload UUID</strong>: UUID of the certificate payload to use for the identity credential.
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td><strong>Roaming Consortium OIs</strong></td>
            <td>Roaming Consortium Organization Identifiers used for Wi-Fi Hotspot 2.0negotiation. Requires 6 or 10 hexadecimal characters.
                <br>Available in iOS 7.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Network Access Identifier ( NAI ) Realm Names</strong></td>
            <td> List of Network Access Identifier Real names used for Wi-Fi Hotspot 2.0 negotiation.
                <br>Available in iOS 7.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Mobile Country Code ( MCC ) / Mobile Network Code ( MNC ) Configuration</strong></td>
            <td> List of Mobile Country Code (MCC)/Mobile Network Code (MNC) pairs used for Wi-Fi Hotspot 2.0 negotiation. Each string must contain exactly six digits.
                <br>Available in iOS 7.0 and later.</td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}