---
bookCollapseSection: true
weight: 19
---

# Login Window Preference

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

These configurations can be used to restrict the app store on a mac-os device. Once this configuration 
profile is installed on a device, corresponding users will not be able to access the app store of the device.

<i>This configuration will be applied only on macOS devices.</i>

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Restrict App Installation.</strong></td>
            <td> Restrict app installations to admin users.
                <br> Available on macOS 10.9 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Restrict app installations to software updates only.</strong></td>
            <td>Restrict app installations to software updates only.
                <br> Available on macOS 10.10 and later</td>
        </tr>
        <tr>
            <td><strong>Disable App Adoption by users.</strong></td>
            <td>Disable App Adoption by users.
                <br> Available on macOS 10.10 and later
            </td>
        </tr>
        <tr>
            <td><strong>Disable software update notifications</strong></td>
            <td>Disable software update notifications.
                <br> Available on macOS 10.10 and later.
            </td>
        </tr>
    </tbody>
</table>


{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}