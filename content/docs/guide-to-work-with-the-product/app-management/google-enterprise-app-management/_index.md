---
bookCollapseSection: true
weight: 2
---


# Google enterprise app management

When enrolling a device as Google enterprise enabled work profile, the work profile will inherit the capabilities to manage apps within the work profile using the Google playstore. Once the server/tenant of the IoT server is configured with Google Enterprise program, the server is able to communicate with the Google play APIs and manage a private <a href="http://play.google.com/work">Google play store</a> for your organisation. Note the admin account for this Google play account is the Google account used when registering for Android enterprise. By enrolling with Android enterprise program, you have automatically created a private space in the Google Playstore to manage both public and private apps for your organisation.

Prior to moving into the concepts of managing apps under Google enterprise, please note the two district entities involved in app management.
<ul style="list-style-type:disc;">
    <li>App management component of Entgra IoT server(Which will be referred here on as App manager) that includes app store and publisher.</li>
    <li>Private Google Playstore</li>
</ul> 

## Approve apps for organisation
When an admin want to push an app in the playstore to a user's device, the first step is to approve the apps that the users are allowed to install. In order to do this Entgra App manager embeds the Google playstore and automatically log to the account of the admin user. Let's look at how this can be achieved.

### Approve public apps
<ul style="list-style-type:disc;">
    <li>Login to publisher web app. For example <a href="https://localhost:9443/publisher">https://localhost:9443/publisher</a></li>
    <li>Go to Manage -> Android Enterprise
    <img src="../../../image/2031.png"  alt="drawing" style="width:75%; border: 1px solid #ddd;" />
    </li>
    <li>Click "Approve applications" button</li>
    <li>Playstore will be popped up and search the app to approve and Click Approve button
    <img src="../../../image/2032.png"  alt="drawing" style="width:50%; border: 1px solid #ddd;"/>
    </li><br>
    <li>Read Read through the follow up dialogs and approve the app. 
    <img src="../../../image/2033.png"  alt="drawing" style="width:50%; border: 1px solid #ddd;"/>
    </li>
    <li>To make the App manager aware of the approvals, the server has to be synced by clicking the sync button. 
    <img src="../../../image/2034.png"  alt="drawing" style="width:50%; border: 1px solid #ddd;"/>
    </li>
    <li>Once the syncing is successful, click the "Apps" button on the top bar to view the newly synced apps</li>
</ul>

### Approve private apps
<ul style="list-style-type:disc;">
    <li>Login to publisher web app. For example <a href="https://localhost:9443/publisher">https://localhost:9443/publisher</a></li>
    <li>Go to Manage -> Android Enterprise
    <img src="../../../image/2031.png"  alt="drawing" style="width:75%; border: 1px solid #ddd;" />
    </li>
    <li>Click "Approve applications" button</li>
    <li>Playstore will be popped up and click private apps from the left side bar. Then click on the + sign at the bottom right of the screen.
    </li>
    <img src="../../../image/2035.png"  alt="drawing" style="width:50%;"/><br>
    <li>Type a title and upload the apk file. Then click create. 
    <img src="../../../image/2036.png"  alt="drawing" style="width:60%; border: 1px solid #ddd;"/>
    </li>
    <li>When app creation is success, go to app list to view the published app.</li>
    <img src="../../../image/2039.png"  alt="drawing" style="width:50%; border: 1px solid #ddd;"/>
    <li>Once the syncing is successful, click the "Apps" button on the top bar to view the newly synced apps</li>
</ul>


## Create Playstore structure

After approving the apps, the admin should also define how the approved app appears on the user's playstore in the device. If this is not done, the user will not see any approved apps. Please read on the pages, cluster and links on <a href="https://developers.google.com/android/work/play/emm-api/store-layout#elements_of_a_store_layout">Google documentation</a> before prceeding.

Created a minimum of one cluster, one page to prior to installing apps. Lets look at how this can be done.
<ul style="list-style-type:disc;">
    <li>Login to publisher web app. For example <a href="https://localhost:9443/publisher">https://localhost:9443/publisher</a></li>
    <li>Go to Manage -> Android Enterprise
    <img src="../../../image/2031.png"  alt="drawing" style="width:75%; border: 1px solid #ddd;" />
    </li>
    <li>Click "Add new page" button and type a name to create a page.</li>
    <li>Next click Add new cluster button.
    </li>
    <img src="../../../image/2040.png"  alt="drawing" style="width:70%;"/><br>
    <li>Title of the cluster can be edited with the button next to cluster. </li>
    <img src="../../../image/2041.png"  alt="drawing" style="width:20%; border: 1px solid #ddd;"/>
    <li>Click on the Add app + button and select apps to be added to the cluster.</li>
    <img src="../../../image/2042.png"  alt="drawing" style="width:70%; border: 1px solid #ddd;"/>
</ul>


## Associate an app with a device/user

In order to associate a device and an app, a <a href="../../device-management-guide/android-device/android-device-policies/">policy has to be defined</a>. At the time the policy is applied, the Google playstore is updated with the selection.

<ul style="list-style-type:disc;">
    <li>Login to devicemgt web app. Android start to create a new Android policy></li>
    <li>Select Enrollment app install</li>
    <img src="../../../image/2043.png"  alt="drawing" style="width:75%; border: 1px solid #ddd;" />
    <li>Select the apps you wish to make available from "Work Profile Availability" column. Note with each tick, a form to define how the app should be installed will be available and make sure to click save with each tick.</li>
    <li>Apply the policy to devices</li>
</ul>

