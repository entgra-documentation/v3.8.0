---
bookCollapseSection: true
weight: 10
---

# Databases

A database is a collection of data structured in an easily accessible manner. Entgra IoT Server is shipped with an H2 database, which serves as the default Carbon database that stores all the related data, e.g., device details and user details. 

The H2 databases enable starting the Entgra IoT Server without performing any configurations. However, it is not recommended to use H2 databases in the production environments that store mission-critical information. 

Following are some important spaces maintained in the Entgra IoT Server databases:

*   **Registry**: The registry is a virtual file system implemented on top of the JDBC layer that is utilized for persisting data and storing configurations. It is partitioned into the following three spaces:
    *   **Local**: This partition contains the system configurations and the runtime data pertaining to a single product instance. This partition is not meant to be shared among multiple servers, hence this is kept in local H2 DB.
    *   **Config**: This partition contains product-specific configurations. These configurations can be shared across multiple instances.
    *   **Governance**: This partition contains data and configurations that are shared across the platform, e.g., WSDLs, schemas, licenses, and APNS certificates.
*   **User Store**

## Database Types

The following table lists out the databases associated with Entgra IoT Server and their criticality.

<table style="width: 83.0263%;">
  <colgroup>
    <col>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Database Type</th>
      <th>Content</th>
      <th>Criticality</th>
    </tr>
    <tr>
      <td><strong>Carbon</strong></td>
      <td>Registry details.</td>
      <td>Not Critical</td>
    </tr>
    <tr>
      <td><strong>Registry</strong></td>
      <td>Configuration and governance registry details.</td>
      <td>Critical</td>
    </tr>
    <tr>
      <td><strong>User Manager</strong></td>
      <td>User permissions, roles and superuser details</td>
      <td>Critical</td>
    </tr>
    <tr>
      <td><strong>CDM</strong></td>
      <td>Core device management data, e.g., device details, device operations, and device policies.</td>
      <td>Critical</td>
    </tr>
    <tr>
      <td><strong>Plugin</strong></td>
      <td>
        <p>These databases hold device type details. By default there are three types of plugin databases:</p>
        <ul>
          <li><strong>Android DB</strong>: Holds Android device details, e.g., magic tokens.</li>
          <li><strong>iOS DB</strong>: Holds iOS device details and push tokens to call APNS.</li>
          <li><strong>Windows DB</strong>: Holds Windows device details.</li>
        </ul>
      </td>
      <td>Critical</td>
    </tr>
    <tr>
      <td><strong>Certificate Management</strong></td>
      <td>The <a href="{{< param doclink >}}using-entgra-iot-server/product-administration/certificate-management/#mutual-ssl-authentication" class="external-link" rel="nofollow">mutual SSL</a> certificate details.</td>
      <td><br></td>
    </tr>
    <tr>
      <td><strong>App Manager</strong></td>
      <td>Application related details.</td>
      <td>Critical</td>
    </tr>
    <tr>
      <td><strong>Store</strong></td>
      <td>Storerelated data.</td>
      <td>Not Critical</td>
    </tr>
    <tr>
      <td><strong>Social</strong></td>
      <td>Data related to application ranking, ratings, and comments.</td>
      <td>Depends on the use case</td>
    </tr>
    <tr>
      <td><strong>Metrics</strong></td>
      <td>
        <p>Metricspertaining to server performance.</p>
      </td>
      <td>Not Critical</td>
    </tr>
  </tbody>
</table>

Database configurations made in a standalone setup are different to the database configurations made in a production-ready distributed setup.

*   **Standalone setup**: The registry and the user manager databases use the same schema in a single SQL file. Therefore, in a standalone setup, the Carbon database acts as both the registry databse and the user manager database containing all three registry spaces, user permissions, and roles.
*   **Distributed setup**:
    *   In a distributed setup we may share the user management data, configurations, and governance registry data with every product instance. Storing all those data within a single Carbon database in not an effective option. As an alternative they are split into three separate schemas, Carbon, registry, and user manager.
    *   All the critical databases mentioned above should be pointed to the database used in the production setup. The non-critical databases can remain in the H2 databases.


## Pointing Product Profiles to Databases

The following diagram depicts how databases should be pointed to Entgra IoT Server profiles in a production-ready setup. 

Make sure to point the core profile and the analytics profile to the same registry and the user manager databases.